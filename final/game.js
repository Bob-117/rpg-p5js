let $ = function (prop) {
    return document.querySelector(prop);
};
let deg_to_rad = function (deg) {
    return deg * (Math.PI / 180);
};
let base_speed = 10;
let x_sensitivity = 0.3;
let y_sensitivity = 0.3;
let x_moment = 0;
let y_moment = 0;
let keys = [];
let cam;
let yAng = 0;
// let floorTexture, wallTexture;
document.addEventListener("mousemove", function (e) {
    x_moment = e.movementX;
    y_moment = e.movementY;
});
let D = {
    cx: 0,
    cy: 0,
    cz: 0,
    x: 0,
    y: 0,
    z: 200,
    r: 0,
};

let main_area = {
    width: 1000,
    length: 1000
}
const obj = {
    box: 50
}

function preload() {

}

function setup() {
    //createCanvas(window.innerWidth, window.innerHeight, WEBGL);
    createCanvas(600, 400, WEBGL);
    cam = createCamera();
}

function draw() {
    background(0);
    noStroke();
    cam.pan(deg_to_rad(-D.cx));
    cam.tilt(deg_to_rad(D.cy));
    D.r -= (x_moment * x_sensitivity);
    yAng -= (y_moment * y_sensitivity);

    cam.setPosition(D.x, -D.y, D.z);

    draw_ground()

    // console.log(D.x, D.y, D.z )
    D.cx = x_moment * x_sensitivity;
    D.cy = y_moment * y_sensitivity;

    if (keys[90]) {
        D.z -= Math.cos(deg_to_rad(D.r)) * base_speed;
        D.x -= Math.sin(deg_to_rad(D.r)) * base_speed;

        // D.z = D.z >  500 ?  Math.cos(deg_to_rad(D.r)) * base_speed : D.z;
        // D.x = D.x >  500 ?  Math.cos(deg_to_rad(D.r)) * base_speed : D.x;
    }
    if (keys[83]) {
        D.z += Math.cos(deg_to_rad(D.r)) * base_speed;
        D.x += Math.sin(deg_to_rad(D.r)) * base_speed;
    }
    if (keys[81]) {
        D.z -= Math.cos(deg_to_rad(D.r + 90)) * base_speed;
        D.x -= Math.sin(deg_to_rad(D.r + 90)) * base_speed;
    }

    if (keys[68]) {
        D.z += Math.cos(deg_to_rad(D.r + 90)) * base_speed;
        D.x += Math.sin(deg_to_rad(D.r + 90)) * base_speed;
    }

    // main_ternary(x_moment !== 0, x_moment > 0, x_moment--,  x_moment++,  x_moment)
    x_moment !== 0 ? (x_moment > 0 ? x_moment-- : x_moment++) : x_moment;
    y_moment !== 0 ? (y_moment > 0 ? y_moment-- : y_moment++) : y_moment;

    if (yAng < -40) {
        y_moment > 0 ? y_sensitivity = 0 : y_sensitivity = 0.15;
    }
    if (yAng > 30) {
        y_moment < 0 ? y_sensitivity = 0 : y_sensitivity = 0.15;
    }
}

