function draw_ground() {
    push()
    fill("white")
    background(200);
    translate(250, 50, 250);
    rotateX(deg_to_rad(90));
    fill(100);
    plane(1000);
    pop();
}