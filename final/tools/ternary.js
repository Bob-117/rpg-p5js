function main_ternary(main_cond, cond, if_true, if_false, default_value) {
    return main_cond ? (cond > 0 ? if_true : if_false) : default_value;
}

