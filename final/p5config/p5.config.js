function keyPressed() {
    keys[keyCode] = true;
}

function keyReleased() {
    keys[keyCode] = false;
}

function mouseClicked() {
    if (canvas.requestPointerLock) {
        canvas.requestPointerLock();
    }
}