const PI = Math.PI;


const obj = {
    box: 50
}
var test = {
    x: 0,
    y: 0
}

const walls = {
    wall1: {

    }
}

let main_area = {
    width: 400,
    length: 400
}

const step = 10

document.onkeydown = checkKey;

function checkKey(e) {
    //document.getElementById("test").innerHTML = "test"
    e = e || window.event;
    //print(e.keyCode);
    if (e.keyCode == '38') {
        test.y - step - obj.box/2 >  - (main_area.width / 2) ?  test.y -= step : null
    }
    else if (e.keyCode == '40') {
        test.y + step + obj.box/2 <   (main_area.width / 2) ?  test.y += step : null
    }
    else if (e.keyCode == '37') {
        console.log(e.keyCode)
        test.y -= 1;
    }
    else if (e.keyCode == '39') {
        console.log(e.keyCode)
        test.y += 1;
    }
}

function setup() {
    createCanvas(600, 600, WEBGL);
    currCamera = createCamera();
}

function draw() {
    rotateX(PI/4);
    // rotateZ(PI/4);
    // orbitControl(10, 10, 10);

    // Map the coordinates of the mouse
    // to the variable
    let cX = map(mouseX, 0,
        width, -200, 200);
    // let cY = map(mouseY, 0,
    //     width, -200, 200);

    // Set the camera to the given coordinates
    // camera(cX, 0, (height/2) / tan(PI/4),
    //     cX, 0, 0, 0, 1, 0);

    console.log(cX)
    currCamera.pan( cX > 0 ? 10 : -10);


    push();
    fill("white")
    background(200);
    plane(main_area.width, main_area.length);
    pop();

    push();
    fill("red");
    translate(main_area.width / 2,0,  obj.box / 2);

    box(5,main_area.width,main_area.length / 10)
    pop()

    // push();
    // translate(-100,60,0);
    // fill("red");
    // box(100,100,30)
    // noFill();
    // pop();

    push();
    noFill();
    translate(0,0,obj.box / 2);
    translate(test.x, test.y, 0);
    box(obj.box)
    noFill();

    pop();
}

